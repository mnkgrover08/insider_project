<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Fixtures;
use App\Model\Groups;
use App\Model\PlayerRoles;
use App\Model\Teams;
use Illuminate\Support\Facades\DB;

class FixturesController extends Controller{

	public function index(Request $request)
	{


		try {
			
			//$fixtures = Fixtures::all();
			$fixtures_count = Fixtures::count();
			if (!$fixtures_count > 0) {
				$this->create_fixtures();
			}

			$fixtures = Fixtures::all()->toArray();

			return response()->json($fixtures);


		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}
		

		/*foreach ($fixtures as $key => $fixture) {
			print_r($fixture);
		}*/
		
		//die('created');

	}


	protected function create_fixtures(){

		try{

			$groups = Groups::all();
			$groups_count = Groups::count();

			$groupwise_teams = [];

			foreach ($groups as $key => $group) {
					$group_id = $group->id;
					$teams = Teams::where('group_id',$group_id)->get();
					$groupwise_teams[$group_id] = $teams;
			}

			$group_a_teams = $groupwise_teams[1];
			$group_b_teams = $groupwise_teams[2];

			foreach ($group_a_teams as $key => $team) {
				$team_1_id = $team->id;
				for ($i=$key+1; $i < count($group_a_teams); $i++) { 
					$team_2_id = $group_a_teams[$i]->id;
					$date_time = date('Y-m-d H:i:s');
					$venue = rand(1,100000);
					$completed = 0;
					$fixtures_instance = new Fixtures;
					$fixtures_instance->team_1_id = $team_1_id;
					$fixtures_instance->team_2_id = $team_2_id;
					$fixtures_instance->date_time = $date_time;
					$fixtures_instance->venue = $venue;
					$fixtures_instance->completed = $completed;
					$fixtures_instance->save();
				}
			}


			foreach ($group_b_teams as $key => $team) {
				$team_1_id = $team->id;
				for ($i=$key+1; $i < count($group_b_teams); $i++) { 
					$team_2_id = $group_b_teams[$i]->id;
					$date_time = date('Y-m-d H:i:s');
					$venue = rand(1,100000);
					$completed = 0;
					$fixtures_instance = new Fixtures;
					$fixtures_instance->team_1_id = $team_1_id;
					$fixtures_instance->team_2_id = $team_2_id;
					$fixtures_instance->date_time = $date_time;
					$fixtures_instance->venue = $venue;
					$fixtures_instance->completed = $completed;
					$fixtures_instance->save();
				}
			}

		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}


	}



}