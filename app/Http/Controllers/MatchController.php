<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Fixtures;
use App\Model\Groups;
use App\Model\PlayerRoles;
use App\Model\Players;
use App\Model\Teams;
use App\Model\Matches;

use Illuminate\Support\Facades\DB;

class MatchController extends Controller{

	public function play_match(Request $request,$id)
	{

		try{

			$match_count = Matches::where('fixture_id',$id)->count();

			if ($match_count>0) {
				return response()->json(['status'=>'Match already played']);			
			}

			$match_fixture = Fixtures::where('id',$id)->get()->toArray()[0];
			/*
				$todo : logic for wining or losing of team
			*/

			$winning_team_id = $match_fixture['team_1_id'];

			$winning_team_players = Players::where('team_associated_id',$winning_team_id)->get();
			$rand_id = rand(1,8);
			$man_of_the_match_id = $winning_team_players[$rand_id]->id;

			$matches_instance = new Matches;

			$matches_instance->fixture_id = $id;
			$matches_instance->winning_team_id = $winning_team_id;
			$matches_instance->winning_team_id = $man_of_the_match_id;
			$matches_instance->save();

			Fixtures::where('id', $id)->update(['completed' => 1]);

			return response()->json(['status'=>'Match Played','winner_team_id'=>$winning_team_id]);	
		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}	
	
	}

	public function index(Request $request){

		try{
			$matches = Matches::all()->toArray();

			return response()->json($matches);		
		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}
	}


}