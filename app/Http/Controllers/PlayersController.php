<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Fixtures;
use App\Model\Groups;
use App\Model\PlayerRoles;
use App\Model\Players;
use Illuminate\Support\Facades\DB;

class PlayersController extends Controller{

	public function index(Request $request)
	{

		try{
			$players = Players::all()->toArray();

			return response()->json($players);
		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}

	}

}