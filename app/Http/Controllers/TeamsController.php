<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Fixtures;
use App\Model\Groups;
use App\Model\PlayerRoles;
use App\Model\Teams;
use Illuminate\Support\Facades\DB;

class TeamsController extends Controller{

	public function index(Request $request)
	{

		try{
			$teams = Teams::all()->toArray();

			return response()->json($teams);
		} catch (Exception $e) {
			return response()->json($e->getMessage());			
		}

	}

}