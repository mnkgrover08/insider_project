<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fixtures extends Model{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fixtures';


}
