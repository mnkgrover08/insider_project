<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PlayerRoles extends Model{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_roles';


}
