<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('matches_played');
            $table->integer('inings_played');
            $table->integer('total_runs');
            $table->integer('highest_score');
            $table->integer('average');
            $table->integer('strike_rate');
            $table->integer('hundereds');
            $table->integer('fifties');
            $table->integer('wickets_taken');
            $table->integer('player_associated_id')->unsigned()->nullable();
            $table->timestamps();

            $table->index('id');
            // foreign
            // $table->foreign('player_associated_id')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_stats');
    }
}
