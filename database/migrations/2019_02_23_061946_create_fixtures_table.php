<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixtures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_1_id')->unsigned()->nullable();
            $table->integer('team_2_id')->unsigned()->nullable();
            $table->string('date_time');
            $table->string('venue');
            $table->integer('completed')->default(0);
            $table->timestamps();
            $table->index('id');
            // foreign
            //$table->foreign('team_1_id')->references('id')->on('teams');
            //$table->foreign('team_2_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixtures');
    }
}
