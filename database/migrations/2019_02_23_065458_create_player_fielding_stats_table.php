<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerFieldingStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_fielding_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('player_id')->unsigned()->nullable();
            $table->integer('match_id')->unsigned()->nullable();
            $table->integer('catches')->unsigned()->nullable();
            $table->timestamps();

            // primary
            $table->unique(['player_id', 'match_id']);

            // foreign
       //     $table->foreign('player_id')->references('id')->on('players');
        //    $table->foreign('match_id')->references('id')->on('matches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_fielding_stats');
    }
}
