<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->unsigned()->nullable();
            $table->integer('total_matches')->unsigned()->nullable();
            $table->integer('matches_won')->unsigned()->nullable();
            $table->integer('matches_lost')->unsigned()->nullable();
            $table->integer('points')->unsigned()->nullable();
            $table->integer('net_run_rate')->unsigned()->nullable();
            $table->timestamps();

            // primary
            $table->unique('team_id');

            // foreign
         //   $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_stats');
    }
}
