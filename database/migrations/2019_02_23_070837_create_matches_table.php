<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fixture_id')->unsigned()->nullable();
            $table->integer('winning_team_id')->unsigned()->nullable();
            $table->integer('man_of_the_match_id')->unsigned()->nullable();
            $table->timestamps();
            $table->index('id');
            // foreign
     //       $table->foreign('fixture_id')->references('id')->on('fixtures');
      //      $table->foreign('winning_team_id')->references('id')->on('teams');
       //     $table->foreign('man_of_the_match_id')->references('id')->on('players');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
