<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $groups = ['A','B'];

    	foreach ($groups as $key => $group) {
    		DB::table('groups')->insert([
	            'name' => $group,
	            'description' => Str::random(10).' Dummy Values',
	            'created_at' => date('Y-m-d H:i:s'),
	            'updated_at' => date('Y-m-d H:i:s')
	        ]);	
    	}
    }
}
