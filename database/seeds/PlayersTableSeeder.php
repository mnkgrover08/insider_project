<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = ['England','India','Bangladesh','Australia','Pakistan','New Zealand','South Africa','Zimbavbe'];
        
        $teams = DB::table('teams')->get()->toArray();

    	foreach ($teams as $key => $team) {
    		for ($i=0; $i <= 15 ; $i++) { 
    			DB::table('players')->insert([
		            'name' => 'Player-'.$team->id.'-'.$i,
		            'age' => 28,
		            'country' => $team->name,
		            'team_associated_id' => $team->id,
		            'created_at' => date('Y-m-d H:i:s'),
	            	'updated_at' => date('Y-m-d H:i:s')
		        ]);
    		}

    	}

    	$players = DB::table('players')->get()->toArray();

    	$roles = ['batsmen','bowler','all_rounder'];
    	$bats = ['right-hand','left-hand'];

    	foreach ($players as $key => $player) {

    		if ($key%3==0) {
    			$role = $roles[0];
    			$bat = $bats[1];	
    		}elseif ($key%2==0) {
    			$role = $roles[1];
    			$bat = $bats[0];
    		}else{
    			$role = $roles[2];
    			$bat = $bats[1];
    		}
    		DB::table('player_roles')->insert([
		            'player_id' => $player->id,
		            'role' => $role,
		            'bats' => $bat,
		            'bowls' => 'medium-pace',
		            'created_at' => date('Y-m-d H:i:s'),
	            	'updated_at' => date('Y-m-d H:i:s')
		        ]);
    	}


    }
}
