<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$teams = ['England','India','Bangladesh','Australia','Pakistan','New Zealand','South Africa','Zimbavbe'];


    	$counter = 1;
    	foreach ($teams as $key => $team) {
    		if ($counter%2==0) {
    			DB::table('teams')->insert([
		            'name' => $team,
		            'description' => Str::random(10).' Dummy Values',
		            'group_id' => 2,
		            'created_at' => date('Y-m-d H:i:s'),
	            	'updated_at' => date('Y-m-d H:i:s')
		        ]);
    		}else{
    			DB::table('teams')->insert([
		            'name' => $team,
		            'description' => Str::random(10).' Dummy Values',
		            'group_id' => 1,
		            'created_at' => date('Y-m-d H:i:s'),
	            	'updated_at' => date('Y-m-d H:i:s')
		        ]);
    		}
    		$counter++;
    	}

   		
    }
}
