## Insider Project ICC trophy Simulation

NGINX Conf:

        server {
                listen 80;
                root /var/www/insider_project/public;
                index index.php index.html index.htm index.nginx-debian.html;
                server_name dev.insider.com;


                add_header X-Frame-Options "SAMEORIGIN";
                add_header X-XSS-Protection "1; mode=block";
                add_header X-Content-Type-Options "nosniff";

                charset utf-8;


                location = /favicon.ico { access_log off; log_not_found off; }
                location = /robots.txt  { access_log off; log_not_found off; }

                error_page 404 /index.php;

                location / {
                        try_files $uri $uri/ /index.php?$query_string;
                }

                location ~ \.php$ {
                        include snippets/fastcgi-php.conf;
                        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
                }

                location ~ /\.ht {
                        deny all;
                }
         
        }

# Host Entry : 127.0.0.1 => dev.insider.com

# Steps for setup:

# git clone https://bitbucket.org/mnkgrover08/insider_project
# composer install
# update Database values in .env
# 777 permissions to storage folder and bootstrap/cache folder
# make nginx conf, restart nginx and then make host entry.

# Now in the root folder, Goto Terminal

# php artisan migrate
# php artisan db:seed --class=GroupsTableSeeder
# php artisan db:seed --class=TeamsTableSeeder
# php artisan db:seed --class=PlayersTableSeeder

# Now seeding in done. 

# Routes for the process:

# http://dev.insider.com/fixtures : This will create the fixtures if they dont exist and then display the fixtures in JSON feed.

# http://dev.insider.com/teams : will give a listing of teams.

# http://dev.insider.com/players : will give a listing of players.

# http://dev.insider.com/play_match/{fixture_id} ex: http://dev.insider.com/play_match/22

# above route will take fixture_id as input and match will be played and corrosponding entry will be made in matches table.
# If match is already played it will show "match already played"

# http://dev.insider.com/matches : This will give the result of matches played with winning team id.

# PS: look at the tables and structures after running migrations. and apologies for not going into Front-end.

