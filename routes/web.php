<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/fixtures','FixturesController@index');

Route::get('/teams','TeamsController@index');

Route::get('/players','PlayersController@index');

Route::get('/play_match/{id}','MatchController@play_match');

Route::get('/matches','MatchController@index');